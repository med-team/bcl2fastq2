/**
 * BCL to FASTQ file converter
 * Copyright (c) 2007-2017 Illumina, Inc.
 *
 * This software is covered by the accompanying EULA
 * and certain third party copyright/licenses, and any user of this
 * source file is bound by the terms therein.
 *
 * \file InteropFile.cpp
 *
 * \brief Implementation of InterOp file.
 *
 * \author Marek Balint
 */


#include <boost/format.hpp>

#include "common/Debug.hh"
#include "common/Logger.hh"
#include "common/SystemCompatibility.hh"
#include "data/InteropFile.hh"


namespace bcl2fastq {


namespace data {


InteropFileState::InteropFileState(const boost::filesystem::path& filePath)
: path_(filePath)
{
}

InteropFileState::~InteropFileState()
{
}

void InteropFileState::write(std::ostream &os, const char *sourceBuffer, std::streamsize sourceSize)
{
    os.write(sourceBuffer, sourceSize);
}


namespace detail {


/// \brief Default InterOp file internal state.
class DefaultInteropFileState : public InteropFileState
{
public:
    DefaultInteropFileState(const boost::filesystem::path& filePath) : InteropFileState(filePath) { }

private:
    virtual void write(std::ostream &os, const char *sourceBuffer, std::streamsize sourceSize);
};

void DefaultInteropFileState::write(std::ostream &os, const char *sourceBuffer, std::streamsize sourceSize)
{
    InteropFileState::write(os, sourceBuffer, sourceSize);
}


} // namespace detail


InteropFile::InteropFile()
: fileBuf_(std::ios_base::out | std::ios_base::app)
{
}

InteropFile::InteropFileStatePtr InteropFile::open(const boost::filesystem::path &path)
{
    io::FileBufWithReopen fileBuf(std::ios_base::out | std::ios_base::trunc);
    // I don't like the addition of this try block. The entire interop file writing implementaion is
    // poorly designed. All we need to do is write a number to a single file. This is over kill.
    try
    {
        fileBuf.reopen(path.native().c_str(), io::FileBufWithReopen::FadviseFlags::SEQUENTIAL_ONCE);
    }
    catch (common::IoError)
    {
        //pass
    }

    if (!fileBuf.is_open())
    {
        BCL2FASTQ_LOG(common::LogLevel::WARNING) << "Unable to open Interop file '" << path.string() << "' for writing" << std::endl;
        return InteropFileStatePtr(NULL);    
    }
    BCL2FASTQ_LOG(common::LogLevel::INFO) << "Created InterOp file '" << path << std::endl;

    return InteropFileStatePtr(new detail::DefaultInteropFileState(path));
}

boost::filesystem::path InteropFile::getPath(const InteropFileState &fileState) const
{
    return fileState.path_;
}

void InteropFile::write(InteropFileState &fileState, const char *sourceBuffer, std::streamsize sourceSize)
{
    fileBuf_.reopen(fileState.path_.native().c_str(), io::FileBufWithReopen::FadviseFlags::SEQUENTIAL_ONCE);
    BCL2FASTQ_ASSERT_MSG(fileBuf_.is_open(), "Unable to reopen InterOp file '" << fileState.path_ << "' for append");
    std::ostream os(&fileBuf_);

    fileState.write(os, sourceBuffer, sourceSize);
}


} // namespace data


} // namespace bcl2fastq


