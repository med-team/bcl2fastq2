/**
 * BCL to FASTQ file converter
 * Copyright (c) 2007-2017 Illumina, Inc.
 *
 * This software is covered by the accompanying EULA
 * and certain third party copyright/licenses, and any user of this
 * source file is bound by the terms therein.
 *
 * \file InteropFile.hh
 *
 * \brief Declaration of InterOp file.
 *
 * \author Marek Balint
 */


#ifndef BCL2FASTQ_DATA_INTEROPFILE_HH
#define BCL2FASTQ_DATA_INTEROPFILE_HH


#include <boost/shared_ptr.hpp>
#include <boost/filesystem/path.hpp>

#include "io/FileBufWithReopen.hh"


namespace bcl2fastq {


namespace data {


class InteropFileState;


/// \brief InterOp file.
class InteropFile
{
public:

    /// \brief Lane number type definition.
    typedef uint16_t LaneNumber;

    /// \brief Tile number type definition.
    typedef uint16_t TileNumber;

    /// \brief Read number type definition.
    typedef uint16_t ReadNumber;

    /// \brief Index name length type definition.
    typedef uint16_t IndexNameLength;

    /// \brief Clusters count type definition.
    typedef uint32_t ClustersCount;

    /// \brief Sample id length type definition.
    typedef uint16_t SampleIdLength;

    /// \brief Project name length type definition.
    typedef uint16_t ProjectNameLength;

    /// \brief InterOp file state smart pointer type definition.
    typedef boost::shared_ptr<InteropFileState> InteropFileStatePtr;

public:

    /// \brief Default constructor.
    InteropFile();

public:

    /// \brief Open InterOp file.
    /// \param path Path to the InterOp file to be opened.
    /// \return External state of opened InterOp file.
    InteropFile::InteropFileStatePtr open(const boost::filesystem::path &path);

public:

    /// \brief Get file path.
    /// \param fileState InterOp file external state.
    /// \return File path.
    boost::filesystem::path getPath(const InteropFileState &fileState) const;

    /// \brief Write bytes from buffer to file.
    /// \param fileState InterOp file external state.
    /// \param sourceBuffer Source buffer to read to.
    /// \param sourceSize Number of bytes to be written.
    void write(InteropFileState &fileState, const char *sourceBuffer, std::streamsize sourceSize);

private:

    /// \brief Filebuf.
    io::FileBufWithReopen fileBuf_;
};


/// \brief InterOp file internal state.
class InteropFileState
{
protected:

    /// \brief Constructor.
    InteropFileState(const boost::filesystem::path& filePath);

    /// \brief Virtual destructor.
    virtual ~InteropFileState() = 0;

protected:

    /// \brief Write bytes from buffer to file.
    /// \param os Output stream to write to.
    /// \param sourceBuffer Source buffer to read to.
    /// \param sourceSize Number of bytes to be written.
    virtual void write(std::ostream &os, const char *sourceBuffer, std::streamsize sourceSize) = 0;

private:

    /// \brief File path.
    boost::filesystem::path path_;

private:

    friend class InteropFile;
};


} // namespace data


} // namespace bcl2fastq


#endif // BCL2INTEROP_DATA_INTEROPFILE_HH


